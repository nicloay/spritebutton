using UnityEngine;
using System.Collections;

public class ButtonManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UFTSpriteBase.onButtonClick+=onButtonClick;
		UFTSpriteBase.onButtonHover+=onButtonHover;
	}

	void onButtonClick (UFTSpriteBase button)
	{
		Debug.Log("button "+ button.name +" has been clicked");
	}

	void onButtonHover (UFTSpriteBase button)
	{
		Debug.Log("button "+button.name +" on hover");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
