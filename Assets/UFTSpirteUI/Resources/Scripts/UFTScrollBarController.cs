using UnityEngine;
using System.Collections;



public delegate void OnPageChange(int pageNumber,UFTScrollBarController scrollBarController);

public class UFTScrollBarController : MonoBehaviour {
	public Transform scrollablePane;
	public OnPageChange onPageChange;
	public Vector3 vectorPerPage;
	public Vector3 paneStartPosition;
	public int currentPage;
	public int numberOfPage;
	
	public float animationSlideTimeInSecond = 0.5f;
	
	void Awake(){
		subscribeToChildrenEvents();
		sendPageChangeNotification(currentPage);
	}	
	
	
	void subscribeToChildrenEvents ()
	{
		foreach (UFTScrollButtonController scrollButton in gameObject.GetComponentsInChildren<UFTScrollButtonController>()){
			scrollButton.onScrollBarOffsetClick+=onScrollBarOffsetClick;
			scrollButton.onScrollBarPageClick+=onScrollBarPageClick;
			scrollButton.subscribeToPageChange(this);
		}
	}

	void onScrollBarOffsetClick (int offset)
	{
		currentPage +=offset;
		scrollPaneToNewPosition();
		sendPageChangeNotification (currentPage);
	}

	void onScrollBarPageClick (int pageNumber)
	{		
		currentPage = pageNumber;
		scrollPaneToNewPosition();	
		sendPageChangeNotification (currentPage);
	}

	void sendPageChangeNotification (int pageNumber)
	{
		if (onPageChange!=null)
			onPageChange(pageNumber, this);
	}
	
	
	private bool doAnimation=false;
	private float startTime;
	private Vector3 scrollDistantion;
	Vector3 startPosition;
		
	void scrollPaneToNewPosition(){
		Vector3 newPosition     = getNewPosition()   ;
		startPosition = scrollablePane.transform.position ;
		scrollDistantion = newPosition - startPosition ;
		startTime= Time.time;
		doAnimation  = true;
		
	}
	
	
	void Update(){
		
		animate();
	}

	void animate ()
	{
		if (!doAnimation)
			return;
		
		float currentTime= Time.time - startTime;
		if (currentTime > animationSlideTimeInSecond){
			doAnimation = false;	
		}
		
		float x = easeOutQuad(currentTime , startPosition.x, scrollDistantion.x, animationSlideTimeInSecond);
		float y = easeOutQuad(currentTime , startPosition.y, scrollDistantion.y, animationSlideTimeInSecond);
		float z = startPosition.z;
		scrollablePane.transform.position = new Vector3(x,y,z);
	}
	
	
	float easeOutQuad  (float currentTime,float startValue,float destValue,float duration) {
		currentTime /= duration;
		return -destValue * currentTime*(currentTime-2) + startValue;
	}
	
	
	private Vector3 getNewPosition(){
		return  paneStartPosition + (vectorPerPage*currentPage);	
	}
}
