using UnityEngine;
using UnityEditor;
using System.Collections;

public class UFTGridSelectorCopySaveMeshAndInitSprites : UFTDataGridSelectorInt {
	public string meshLocationRelativeToAssetFolder="";
	public UFTAtlasMetadata atlasMetadata;
	public UFTClippingRect clippingRect;
	
	[MenuItem("Window/UFT Grid selectors/tempOne")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<UFTGridSelectorCopySaveMeshAndInitSprites> ();
	}
	
	
	#region implemented abstract members of UFTDataGridSelectorInt
	public override void initializeItem (Object prefab, System.Object metaObject, Vector3 position, UFTClippingRect clippingRect, int index)
	{
		Transform transforms=((GameObject)prefab).GetComponentInChildren<Transform>();
		foreach (Transform tr in transforms) {					
			MeshFilter mf=tr.GetComponent<MeshFilter>();
			
			if (mf != null){
				Mesh mesh=mf.sharedMesh;
				if (mesh != null){
					Mesh clonedMesh = UFTMeshUtil.cloneMesh(mesh);				
					mf.sharedMesh = clonedMesh;					
					string newMeshPath="Assets/meshClone_"+index;
					string meshPath=AssetDatabase.GetAssetPath(mesh);
					if (!meshPath.Equals(".")){												
						newMeshPath= meshPath.Insert((meshPath.LastIndexOf(".")), "_"+index);						
					}
					AssetDatabase.CreateAsset(clonedMesh,newMeshPath);
					AssetDatabase.SaveAssets();
					
					
				}				
			}
			UFTSpriteBase spriteBase=tr.GetComponent<UFTSpriteBase>();
			if (spriteBase != null){				
				spriteBase.atlasMetadata = atlasMetadata;				
			}
			UFTSprite uftSprite= tr.GetComponent<UFTSprite>();
			if (uftSprite !=null){
				uftSprite.spriteMetadata =((UFTGraffitiMetadata) metaObject).atlasEntryMetadata;
				uftSprite.onAtlasMigrate();
			}
			
			if (clippingRect!=null && tr.GetComponent<Renderer>()!=null){
				UFTClippingItem ci= tr.gameObject.AddComponent<UFTClippingItem>();
				ci.clippingRect=clippingRect;
			}
			 
		}
	}
	#endregion
	
}
