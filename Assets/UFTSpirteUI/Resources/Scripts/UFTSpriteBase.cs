using UnityEngine;
using System.Collections;


public delegate void On3LayerButtonClick(UFTSpriteBase button);
public delegate void On3LayerButtonHover(UFTSpriteBase button);

public class UFTSpriteBase : UFTMonoBehaviour {
	public UFTAtlasMetadata atlasMetadata;

	public static On3LayerButtonClick onButtonClick;
	public static On3LayerButtonHover onButtonHover;
	
	
	protected sealed override void OnMouseEnter(){		
		if (onButtonHover != null)
			onButtonHover(this);
	}
	
	public  void OnMouseDown(){
		if (onButtonClick != null)
			onButtonClick(this);
	}
	
	/*
	 *  triangles on plane
	 *	1,5 	4
	 * 	
	 *	0		2,3 
	 */
	
	public void applyMetadata(UFTAtlasEntryMetadata metadata){
		Mesh sharedMesh= renderer.GetComponent<MeshFilter>().sharedMesh;	
		Vector2[] uvs=sharedMesh.uv;
		Rect rect=metadata.uvRect;
		uvs[0]   =        new Vector2( rect.x             , rect.y);
		uvs[1] = uvs[5] = new Vector2( rect.x             , rect.y + rect.height);
		uvs[4]   =        new Vector2( rect.x + rect.width, rect.y + rect.height);
		uvs[2] = uvs[3] = new Vector2( rect.x + rect.width, rect.y);
		sharedMesh.uv=uvs;
	}
}
