using UnityEngine;
using System.Collections;
using System;


public class UFTFlexibleBorderSprite : UFTSpriteBase, UFTOnAtlasMigrateInt {	
	public UFTAtlasEntryMetadata spriteMetadata;
	
	public UFTBorderWidth uvBorderWidth;
	public UFTBorderWidth vertexBorderWidth;
	
	public float width;
	public float height;
	
	
	public void updateMesh(){
		Mesh mesh=gameObject.GetComponent<MeshFilter>().sharedMesh;
		
	}
	
	
	public void syncMeshWithSprite(){
		Mesh mesh=UFTFlexibleBorderMeshUtil.createBorderMesh(width,height, vertexBorderWidth,uvBorderWidth,spriteMetadata);
		gameObject.GetComponent<MeshFilter>().sharedMesh=mesh;
	}
	
	#region UFTOnAtlasMigrateInt implementation
	public void onAtlasMigrate ()
	{
		syncMeshWithSprite();
	}
	#endregion
}
