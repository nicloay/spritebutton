using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class UFTClippingItem : MonoBehaviour {
	public UFTClippingRect clippingRect;
	public Matrix4x4 matrix;
	
	public bool showVertexGizmos=false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//#if UNITY_EDITOR//
		updateMatrixAndMaterial();
//#endif
	}

	void updateMatrixAndMaterial ()
	{
		if (clippingRect!=null){

			matrix =clippingRect.transform.worldToLocalMatrix;
			
			Material mat=transform.renderer.sharedMaterial;
			mat.SetMatrix("MyMatrix",matrix);
			mat.mainTextureOffset = new Vector2(-clippingRect.width/2, -clippingRect.height/2);
			mat.mainTextureScale = new Vector2(clippingRect.width, clippingRect.height);			
		}else{
			Debug.LogError("clippingRect is empty");
		}
	}
	
	void OnDrawGizmos (){
		debugMatrix ();
		
	}

	void debugMatrix ()
	{
		if (!showVertexGizmos)
			return;
		if (matrix!=null){
			Gizmos.matrix=matrix;
			MeshFilter mf=gameObject.GetComponent<MeshFilter>();
			Mesh mesh=mf.sharedMesh;
			foreach (Vector3 vertex in mesh.vertices){
				Vector3 point=new Vector3(vertex.x,vertex.y,0);
				Gizmos.DrawSphere(vertex,0.1f);
			}	
		}
	}
}
