using UnityEngine;
using System.Collections;

public class UFTClippingRect : MonoBehaviour {
	public float width=100;
	public float height=100;
	
	void OnDrawGizmos ()
	{
		Gizmos.matrix=transform.localToWorldMatrix;
		float halfWidth=width/2;
		float halfHeight=height/2;
		Vector3 topLeft     = new Vector3( -halfWidth,  halfHeight, 0);
		Vector3 bottomLeft  = new Vector3( -halfWidth, -halfHeight, 0);
		Vector3 topRight    = new Vector3(  halfWidth,  halfHeight, 0);
		Vector3 bottomRight = new Vector3(  halfWidth, -halfHeight, 0);
		
		Gizmos.DrawLine(topLeft,topRight);
		Gizmos.DrawLine(bottomLeft,bottomRight);
		Gizmos.DrawLine(topLeft,bottomLeft);
		Gizmos.DrawLine(topRight,bottomRight);
		
		
	}
}
