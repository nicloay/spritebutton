using UnityEngine;
using System.Collections;
using UnityEditor;



[CustomEditor(typeof(UFTFlexibleBorderSprite))]
public class UFTFlexibleBorderSpriteEditor : Editor {
	bool syncUV=true;
	
	public override void OnInspectorGUI(){
		bool needUpdateMesh=false;
		UFTFlexibleBorderSprite targetObject= (UFTFlexibleBorderSprite)target;		
		UFTAtlasEntryMetadata newEntryMeta = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("sprite"  , targetObject.atlasMetadata, targetObject.spriteMetadata);
		if (newEntryMeta != targetObject.spriteMetadata){
			targetObject.spriteMetadata =newEntryMeta;
			needUpdateMesh=true;
		}
		
		EditorGUI.BeginChangeCheck();
		targetObject.width=EditorGUILayout.IntField("width",(int)targetObject.width);		
		targetObject.height=EditorGUILayout.IntField("height",(int)targetObject.height);		
		
		drawBorderWidth("UV Border Width",ref targetObject.uvBorderWidth);
		syncUV = EditorGUILayout.Toggle("sync UV width with Vertex", syncUV);
		if (syncUV){
			targetObject.vertexBorderWidth.getValuesFrom(targetObject.uvBorderWidth);
			GUI.enabled=false;
		} else{
			GUI.enabled=true;	
		}
		
		drawBorderWidth("Vertex Border Width",ref targetObject.vertexBorderWidth);
		GUI.enabled = true;
		
		
		
		if (needUpdateMesh || EditorGUI.EndChangeCheck()){
			targetObject.syncMeshWithSprite();
		}
	}
	
	void drawBorderWidth(string caption, ref UFTBorderWidth borderWidth){
		EditorGUILayout.LabelField(caption+":");
		borderWidth.top=EditorGUILayout.IntField("top",(int) borderWidth.top);
		borderWidth.down=EditorGUILayout.IntField("down",(int) borderWidth.down);
		borderWidth.left=EditorGUILayout.IntField("left",(int) borderWidth.left);
		borderWidth.right=EditorGUILayout.IntField("right",(int) borderWidth.right);
		
	}
	
}
