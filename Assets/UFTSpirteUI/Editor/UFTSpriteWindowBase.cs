using UnityEngine;
using System.Collections;
using UnityEditor;

public abstract class UFTSpriteWindowBase : EditorWindow {

	public UFTAtlasMetadata atlasMetadata;
	public Material buttonMaterial;
	public string buttonName;
	
			
	public int width;
	public int height;
	
	private static string PLAYERPREFS_ATLASMETA_KEY="uftLayerButton.atlasmeta";
	private static string PLAYERPREFS_BUTTON_MATERIAL="uftLayerButton.material";
	
	
	public virtual void initializeAtlasMetadataAndMaterial(){		
		string atlasAssetPath=EditorPrefs.GetString(PLAYERPREFS_ATLASMETA_KEY,null);
		if (atlasAssetPath !=null){
			atlasMetadata = (UFTAtlasMetadata) AssetDatabase.LoadAssetAtPath(atlasAssetPath,typeof(UFTAtlasMetadata));
		}
		string buttonMaterialAssetPath=EditorPrefs.GetString(PLAYERPREFS_BUTTON_MATERIAL,null);
		if (buttonMaterialAssetPath != null){
			buttonMaterial=	 (Material) AssetDatabase.LoadAssetAtPath(buttonMaterialAssetPath,typeof(Material));
		}		
	}
	
	public void renderAtlasMetadata ()
	{
		UFTAtlasMetadata newAtlasMetadata = (UFTAtlasMetadata) EditorGUILayout.ObjectField(atlasMetadata,typeof(UFTAtlasMetadata),false);		
		if (newAtlasMetadata !=atlasMetadata){
			atlasMetadata=newAtlasMetadata;
			EditorPrefs.SetString(PLAYERPREFS_ATLASMETA_KEY,AssetDatabase.GetAssetPath(atlasMetadata));
		}	
		
		
		GUI.enabled = (atlasMetadata != null);
	}

	public void renderSizeMaterialGenerateButton ()
	{
		width = EditorGUILayout.IntField("width", width);
		height = EditorGUILayout.IntField("height", height);
		
		Material newButtonMaterial=(Material) EditorGUILayout.ObjectField(buttonMaterial,typeof(Material),false);
		if (newButtonMaterial != buttonMaterial){
			buttonMaterial=newButtonMaterial;
			EditorPrefs.SetString(PLAYERPREFS_BUTTON_MATERIAL,AssetDatabase.GetAssetPath(buttonMaterial));
		}	
		
		buttonName=EditorGUILayout.TextField("button name",buttonName);		
		
		GUI.enabled = isAllMandatoryOptionForGenerateButtonSet();
		Rect buttonRect=EditorGUILayout.BeginHorizontal("Button");
		if ( GUI.Button(buttonRect, GUIContent.none) ){
			onGenerateButton();
			Close ();
		}		
		GUILayout.Label ("Create Button");
		EditorGUILayout.EndHorizontal();
	}
	

	
	public bool isAllMandatoryOptionForGenerateButtonSet(){
		return atlasMetadata  !=null &&
			   buttonName     !=null &&
			   buttonMaterial !=null;			  
	}	

	public T createAndSaveObject<T> (bool addMeshCollider=true) where T:UFTSpriteBase
	{
	 	GameObject go = UFTMeshUtil.createPlane(width,height);
		if(addMeshCollider)
			go.AddComponent<MeshCollider>();		
		T sprite=go.AddComponent<T>();		
		sprite.atlasMetadata=atlasMetadata;		
		go.renderer.material=buttonMaterial;
		go.name=buttonName;
		AssetDatabase.CreateAsset(go.GetComponent<MeshFilter>().sharedMesh, AssetDatabase.GenerateUniqueAssetPath("Assets/"+buttonName+".asset") );
        AssetDatabase.SaveAssets();	
		return sprite;
	}	
	
	public void showMetadataGui(ref UFTAtlasEntryMetadata atlasEntryMeta, string caption){
		UFTAtlasEntryMetadata newAtlasEntryMeta = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption(caption,atlasMetadata, atlasEntryMeta, this.GetInstanceID());
		if (newAtlasEntryMeta !=atlasEntryMeta ){
			atlasEntryMeta=newAtlasEntryMeta;
			width=(int) atlasEntryMeta.pixelRect.width;
			height=(int) atlasEntryMeta.pixelRect.height;
			string name=atlasEntryMeta.name;
			buttonName= name.Substring(0,name.LastIndexOf("."));
			onAtlasEntryChange();
		}
		
	}
	
	public virtual void onGenerateButton(){
		
	}
	
	public virtual void onAtlasEntryChange(){
		
	}
	
}
