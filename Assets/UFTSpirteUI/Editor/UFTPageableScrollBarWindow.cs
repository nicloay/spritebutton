using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFTPageableScrollBarWindow : UFTSpriteWindowBase {
	
	private UFTAtlasEntryMetadata leftButtonSprite;
	private UFTAtlasEntryMetadata pageSprite;	
	private UFTAtlasEntryMetadata rightButtonSprite;
	
	private UFTAtlasEntryMetadata leftButtonSpritePassive;
	private UFTAtlasEntryMetadata pageSpritePassive;	
	private UFTAtlasEntryMetadata rightButtonSpritePassive;
	
	private GameObject scrollPlane;
	private Bounds planeBounds;
	private Vector3 vectorPerPage;
	private int numberOfPages;
	private float pageButtonOffset;
	private Material buttonsMaterial;
	
	
	[MenuItem ("Window/UFT Pageable ScrollBar")]
    static void ShowWindow () {    		
		UFTPageableScrollBarWindow window= EditorWindow.GetWindow <UFTPageableScrollBarWindow>("UFT Pageable ScrollBar");		
    }
	
	
	void OnGUI(){		
		renderAtlasMetadata();
		
		EditorGUILayout.BeginHorizontal();
		showMetadataGui(ref leftButtonSprite  , "Left Button"  );
		showMetadataGui(ref pageSprite        , "Page Sprite"  );
		showMetadataGui(ref rightButtonSprite , "Right Button" );		
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		showMetadataGui(ref leftButtonSpritePassive  , "Left Button Passive"  );
		showMetadataGui(ref pageSpritePassive        , "Page Sprite Passive"  );
		showMetadataGui(ref rightButtonSpritePassive , "Right Button Passive" );		
		EditorGUILayout.EndHorizontal();
		
		buttonMaterial = (Material) EditorGUILayout.ObjectField(buttonMaterial, typeof(Material),false);
		
		
		GameObject newValue =(GameObject) EditorGUILayout.ObjectField(scrollPlane,typeof(GameObject),true);	
		if (newValue != scrollPlane ){
			scrollPlane = newValue;
			planeBounds= recalculateBounds(scrollPlane);	
		}
		
		GUI.enabled = (scrollPlane != null);
		EditorGUILayout.BoundsField("scroll plane bounds (readonly):",planeBounds);
		EditorGUILayout.Separator();
		Vector3 newVectorValue=EditorGUILayout.Vector3Field("Scroll Page Length",vectorPerPage);
		if (newVectorValue!=vectorPerPage){
			vectorPerPage=newVectorValue;
			recalculateNumberOfPages();
		}
		
		EditorGUILayout.IntField("Number of pages (readonly):",numberOfPages);
		pageButtonOffset = EditorGUILayout.FloatField("Page Button Offset:",pageButtonOffset);
		EditorGUILayout.Separator();
		if (GUILayout.Button("generate scrollbar objects"))
			generateScrollBar();
		
	}
	
	Bounds recalculateBounds (GameObject scrollPlane)
	{		
		Bounds bounds=new Bounds();
		Renderer[] children=scrollPlane.GetComponentsInChildren<Renderer>();
		foreach(Renderer r in children)
			bounds.Encapsulate(r.bounds);
		return bounds;
	}

	void recalculateNumberOfPages ()
	{
		int horizontal = -1;
		if (vectorPerPage.x !=0)
			horizontal =Mathf.CeilToInt (planeBounds.max.x / vectorPerPage.x) ;
		
		int vertical = -1;
		if (vectorPerPage.y != 0)
			vertical   =Mathf.CeilToInt (planeBounds.max.y / vectorPerPage.y) ;
		
		if (horizontal >0 && vertical >0 ){
			numberOfPages = Mathf.Min(horizontal, vertical);
		} else {
			int tmp=Mathf.Max(horizontal,vertical);
			numberOfPages = tmp >0? tmp : 0;
		}
	}

	void generateScrollBar ()
	{
		if (numberOfPages<1)
			throw new System.Exception("number of page must be greater than 0");
			
		
		GameObject parent = new GameObject("scrollBar_"+scrollPlane.name);
		UFTScrollBarController scrollBarController = parent.AddComponent<UFTScrollBarController>();
		scrollBarController.vectorPerPage          = vectorPerPage                  ;
		scrollBarController.scrollablePane         = scrollPlane.transform          ;
		scrollBarController.paneStartPosition      = scrollPlane.transform.position ;
		scrollBarController.numberOfPage           = numberOfPages                  ;
		float step = pageSprite.pixelRect.width + pageButtonOffset;
		float leftPagePosition = -((float)((numberOfPages-1) * step)) / 2;		
		
		
		UFT2LayerSprite leftButton=createAndSaveObject<UFT2LayerSprite>(true);					
		leftButton.mainStateEntryMetadata  = leftButtonSprite         ;
		leftButton.spareStateEntryMetadata = leftButtonSpritePassive  ;
		leftButton.name                    = "previous"               ;
		leftButton.transform.parent        = parent.transform         ;
		leftButton.transform.renderer.sharedMaterial = buttonMaterial ;
		leftButton.onAtlasMigrate();
		UFTScrollButtonController leftButtonController = leftButton.gameObject.AddComponent<UFTScrollButtonController>();
		leftButtonController.spriteController          = leftButton           ;
		leftButtonController.scrollType                = ScrollType.BY_OFFSET ;
		leftButtonController.offset                    = -1                   ;
		leftButton.transform.position                  = 
			new Vector3(leftPagePosition - leftButtonSprite.pixelRect.width - pageButtonOffset, 
				        0                                                                     , 
				        0                                                                     );
		
		UFT2LayerSprite rightButton=createAndSaveObject<UFT2LayerSprite>(true);		
		rightButton.mainStateEntryMetadata   = rightButtonSprite        ;		
		rightButton.spareStateEntryMetadata  = rightButtonSpritePassive ;
		rightButton.name                     = "next"                   ;
		rightButton.transform.parent         = parent.transform         ;
		rightButton.transform.renderer.sharedMaterial = buttonMaterial  ;
		rightButton.onAtlasMigrate();
		UFTScrollButtonController rightButtonController = rightButton.gameObject.AddComponent<UFTScrollButtonController>();
		rightButtonController.spriteController          = rightButton          ;
		rightButtonController.scrollType                = ScrollType.BY_OFFSET ;
		rightButtonController.offset                    = +1                   ;
		rightButton.transform.position                  = 
			new Vector3(-leftPagePosition + leftButtonSprite.pixelRect.width + pageButtonOffset,
				        0                                                                      , 
				        0                                                                      );
		
		
		GameObject pagesRoot= new GameObject("pages");
		pagesRoot.transform.parent = parent.transform;
		
		
		
		float pageX = leftPagePosition;
		for (int i = 0; i < numberOfPages; i++) {
			UFT2LayerSprite pageButton=createAndSaveObject<UFT2LayerSprite>(true);		
			pageButton.mainStateEntryMetadata  = pageSprite               ;		
			pageButton.spareStateEntryMetadata = pageSpritePassive        ;
			pageButton.name                    = "page_" + i              ;
			pageButton.transform.parent        = pagesRoot.transform      ;
			pageButton.transform.renderer.sharedMaterial = buttonMaterial ;
			pageButton.onAtlasMigrate();	
			UFTScrollButtonController scrollButtonController = pageButton.gameObject.AddComponent<UFTScrollButtonController>();
			scrollButtonController.spriteController          = pageButton          ;
			scrollButtonController.scrollType                = ScrollType.BY_INDEX ;
			scrollButtonController.pageNumber                = i                   ;						
			pageButton.transform.position=new Vector3(pageX,0,0);
			pageX += step;
		}
		
	}
}
