using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(UFTSprite))]
public class UFTSpriteEditor : Editor {
	private bool changeSize=false;
	private Vector2 newSize;
	private bool useCollider;
	
	void OnEnable () {
        UFTSprite targetObject= (UFTSprite)target;
		useCollider=(targetObject.gameObject.GetComponent<MeshCollider>() != null) ;
    }
	
	public override void OnInspectorGUI(){
		UFTSprite targetObject=(UFTSprite)target;
		GUI.enabled=(targetObject.atlasMetadata!=null);		
		EditorGUILayout.ObjectField("atlasMetadata", targetObject.atlasMetadata,typeof(UFTAtlasEntryMetadata));
		
		UFTAtlasEntryMetadata newEntryMeta = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("normal"  , targetObject.atlasMetadata,targetObject.spriteMetadata);
		if (newEntryMeta != targetObject.spriteMetadata){
			targetObject.spriteMetadata =newEntryMeta;
			targetObject.applyMetadata(targetObject.spriteMetadata);
		}
		bool newUseCollider=EditorGUILayout.Toggle("use collider",useCollider);
		if (newUseCollider != useCollider){
			if (newUseCollider == true ){
				if (targetObject.gameObject.GetComponent<MeshCollider>() == null )
					targetObject.gameObject.AddComponent<MeshCollider>();	
			} else {
				if (targetObject.gameObject.GetComponent<MeshCollider>() != null )
					GameObject.Destroy(targetObject.gameObject.GetComponent<MeshCollider>());				
			}
			
			
			
			useCollider=newUseCollider;	
		}
		
		EditorGUILayout.Vector2Field("current size",UFTMeshUtil.getPlaneObjectSize(targetObject.gameObject));
		
		
		bool newChangeSize = EditorGUILayout.Foldout(changeSize,"change mesh size");
		if (newChangeSize == true && changeSize==false){
			newSize=getSizeFromState(targetObject.spriteMetadata);
		}		
		changeSize=newChangeSize;			
		if (changeSize)
			showChangeSizeGUIBlock(targetObject);
	}
	
	private Vector2 getSizeFromState(UFTAtlasEntryMetadata entryMetadata){
		return new Vector2( entryMetadata.pixelRect.width, entryMetadata.pixelRect.height);	
	}
	
	
	
	public void showChangeSizeGUIBlock (UFTSprite targetObject)
	{		
		
		Rect buttonRect1=EditorGUILayout.BeginHorizontal();
		if ( GUI.Button(buttonRect1, GUIContent.none) ){
			newSize=getSizeFromState(targetObject.spriteMetadata);
		}		
		GUILayout.Label ("Copy Plane Size from sprite");
		EditorGUILayout.EndHorizontal();
		
		
		newSize = EditorGUILayout.Vector2Field ("new size",newSize);
		
		Rect buttonRect2=EditorGUILayout.BeginHorizontal();
		if ( GUI.Button(buttonRect2, GUIContent.none) ){
			updateMeshSize(targetObject);			
		}		
		GUILayout.Label ("Update Mesh Size");
		EditorGUILayout.EndHorizontal();
	}

	public void updateMeshSize (UFTSprite targetObject)
	{
		UFTMeshUtil.updateMesh(targetObject.gameObject, newSize.x, newSize.y);
		targetObject.applyMetadata(targetObject.spriteMetadata);
	}
	
}
