using UnityEngine;
using System.Collections;
using UnityEditor;



[CustomEditor(typeof(UFT3LayerButton))]
public class UFT3LayerButtonEditor : Editor {
	private Vector2 scrollPosition;
	private bool changeSize=false;
	private Vector2 newSize;
 	private static string[] layerOptions= new string[] {"normal", "onHover", "onClick"};	
	
	
	public override void OnInspectorGUI(){
		UFT3LayerButton targetObject=(UFT3LayerButton)target;
		GUI.enabled=(targetObject.atlasMetadata!=null);		
		EditorGUILayout.ObjectField("atlasMetadata", targetObject.atlasMetadata,typeof(UFTAtlasEntryMetadata));
		scrollPosition=EditorGUILayout.BeginScrollView(scrollPosition,GUILayout.Height(170));
		EditorGUILayout.BeginHorizontal();		
		
		UFTAtlasEntryMetadata newEntryMeta = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("normal"  , targetObject.atlasMetadata,targetObject.normalStateEntryMetadata);
		if (newEntryMeta != targetObject.normalStateEntryMetadata){
			targetObject.normalStateEntryMetadata =newEntryMeta;
			targetObject.applyMetadata(targetObject.normalStateEntryMetadata);
		}
		
		targetObject.onHoverStateEntryMetadata = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("on Hover", targetObject.atlasMetadata,targetObject.onHoverStateEntryMetadata);
		targetObject.onClickStateEntryMetadata = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("on Click", targetObject.atlasMetadata,targetObject.onClickStateEntryMetadata);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndScrollView();
		EditorGUILayout.Vector2Field("current size",UFTMeshUtil.getPlaneObjectSize(targetObject.gameObject));
		
		
		bool newChangeSize = EditorGUILayout.Foldout(changeSize,"change mesh size");
		if (newChangeSize == true && changeSize==false){
			newSize=getSizeFromState(targetObject.normalStateEntryMetadata);
		}		
		changeSize=newChangeSize;			
		if (changeSize)
			showChangeSizeGUIBlock(targetObject);
	}
	
	private Vector2 getSizeFromState(UFTAtlasEntryMetadata entryMetadata){
		return new Vector2( entryMetadata.pixelRect.width, entryMetadata.pixelRect.height);	
	}
	
	
	
	public void showChangeSizeGUIBlock (UFT3LayerButton targetObject)
	{		
		
		int choosenOne = EditorGUILayout.Popup("readObject from button", -1, layerOptions);
		
		switch (choosenOne) {
		case 0:
			newSize=getSizeFromState(targetObject.normalStateEntryMetadata);
			break;
		case 1:
			newSize=getSizeFromState(targetObject.onHoverStateEntryMetadata);
			break;			
		case 2:
			newSize=getSizeFromState(targetObject.onClickStateEntryMetadata);
			break;			
		}
		
		newSize = EditorGUILayout.Vector2Field ("new size",newSize);
		
		Rect buttonRect=EditorGUILayout.BeginHorizontal();
		if ( GUI.Button(buttonRect, GUIContent.none) ){
			updateMeshSize(targetObject);			
		}		
		GUILayout.Label ("Update Mesh Size");
		EditorGUILayout.EndHorizontal();
	}

	public void updateMeshSize (UFT3LayerButton targetObject)
	{
		UFTMeshUtil.updateMesh(targetObject.gameObject, newSize.x, newSize.y);
		targetObject.applyMetadata(targetObject.normalStateEntryMetadata);
	}
}
