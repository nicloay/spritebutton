using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFT2LayerSpriteWindow : UFTSpriteWindowBase {
	private UFTAtlasEntryMetadata mainStateEntryMetadata;
	private UFTAtlasEntryMetadata spareStateEntryMetadata;
	private bool isAllTexturesHasSameSize=false;
	
	[MenuItem ("Window/UFT 2Layer sprite")]
    static void ShowWindow () {    		
		UFT2LayerSpriteWindow window= EditorWindow.GetWindow <UFT2LayerSpriteWindow>("UFT 2Layer Sprite");
		window.initializeAtlasMetadataAndMaterial();
    }
	
	
	
	void OnGUI(){		
		renderAtlasMetadata();
		
		EditorGUILayout.BeginHorizontal();
		showMetadataGui(ref mainStateEntryMetadata,"Main State");
		showMetadataGui(ref spareStateEntryMetadata,"Spare State");				
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Toggle("all textures has the same size",isAllTexturesHasSameSize);
		
		renderSizeMaterialGenerateButton();
		
	}
	
	public override void onGenerateButton(){
		UFT2LayerSprite button=createAndSaveObject<UFT2LayerSprite>();
		button.mainStateEntryMetadata = mainStateEntryMetadata;
		button.spareStateEntryMetadata = spareStateEntryMetadata;
		button.onAtlasMigrate();
	}
	
	public override void onAtlasEntryChange(){
		isAllTexturesHasSameSize=isAllStateHasSameSize();
	}
	
	private bool isAllStateHasSameSize(){
		return mainStateEntryMetadata !=null &&  
				spareStateEntryMetadata !=null &&
				mainStateEntryMetadata.pixelRect.width == spareStateEntryMetadata.pixelRect.width ;				
	}
}
