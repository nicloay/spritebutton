using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;


[CustomEditor(typeof(UFTDataGrid))]
public class UFTDataGridEditor : Editor {

	public override void OnInspectorGUI(){
		base.OnInspectorGUI();		
		if (GUILayout.Button("regenerate grid")){
			((UFTDataGrid)target).regenerateGrid();
		}		
		
		
		if (GUILayout.Button("add clipping rect")){
			addClippingRect();
		}
		
		
		if (GUILayout.Button("destroy all children")){
			List<GameObject> toDelete = new List<GameObject>();
			
			foreach(Transform go in ((UFTDataGrid)target).gameObject.transform){
				toDelete.Add(go.gameObject);
			}
			
			toDelete.ForEach(child => DestroyImmediate(child));
			
		}	
		
	}

	public void addClippingRect ()
	{
		UFTDataGrid targetObject= (UFTDataGrid)target;
		UFTClippingRect clipRect= targetObject.gameObject.AddComponent<UFTClippingRect>();
		clipRect.width=800;
		clipRect.height=600;
		
		foreach (Transform t in   targetObject.GetComponentsInChildren<Transform>()){
			if (t.gameObject.GetComponent<Renderer>()!=null){
				UFTClippingItem item= t.gameObject.AddComponent<UFTClippingItem>();
				item.clippingRect=clipRect;			
			}
		}
	}
}
