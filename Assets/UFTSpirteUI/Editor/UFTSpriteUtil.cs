using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFTSpriteUtil  {
	
	public static float SPRITE_RECT_WIDTH  = 100.0f ;
	public static float SPRITE_RECT_HEIGH  = 130.0f ;
	public static float SPRITE_MARGIN      = 5.0f   ;
	public static float SPRITE_TEXT_HEIGHT = 15.0f  ;
	
	private static GUIStyle _rectStyle;
	public static GUIStyle rectStyle {
		get {
			if (_rectStyle==null){
				_rectStyle=new GUIStyle();
				_rectStyle.fixedWidth=SPRITE_RECT_WIDTH+(SPRITE_MARGIN*2);
				_rectStyle.margin=new RectOffset((int)SPRITE_MARGIN, (int)SPRITE_MARGIN, (int)SPRITE_MARGIN, (int)SPRITE_MARGIN);
			}
			return _rectStyle;
		}
		set {
			_rectStyle = value;
		}
	}	
	
	private static GUIStyle _boxStubStyle;
	public static GUIStyle boxStubStyle {
		get {
			if (_boxStubStyle == null){								
				_boxStubStyle = new GUIStyle(GUI.skin.GetStyle("Box"));				
				_boxStubStyle.alignment=TextAnchor.MiddleCenter;
				_boxStubStyle.fontSize=70;
			}			
			return _boxStubStyle;
		}		
	}
	
	private static GUIStyle _middleCenterLabelStyle;
	public static GUIStyle middleCenterLabelStyle {
		get {
			if (_middleCenterLabelStyle == null){								
				_middleCenterLabelStyle = new GUIStyle(GUI.skin.GetStyle("Label"));				
				_middleCenterLabelStyle.alignment = TextAnchor.MiddleCenter;				
			}			
			return _middleCenterLabelStyle;
		}		
	}
	
	private static GUIStyle _middleLeftLabelStyle;
	public static GUIStyle middleLeftLabelStyle {
		get {
			if (_middleLeftLabelStyle == null){
				_middleLeftLabelStyle = new GUIStyle( GUI.skin.GetStyle("Label"));
				_middleLeftLabelStyle.alignment=TextAnchor.MiddleLeft;				
			}
			return _middleLeftLabelStyle;
		}
		
	}		
	
	private static GUIStyle _verticalScrollViewFixedWidthStyle;
	public static GUIStyle verticalScrollViewFixedWidthStyle {
		get {
			if (_verticalScrollViewFixedWidthStyle==null){				
				_verticalScrollViewFixedWidthStyle=new GUIStyle(GUI.skin.FindStyle("ScrollView"));
				_verticalScrollViewFixedWidthStyle.margin=new RectOffset(0,0,0,0);
				_verticalScrollViewFixedWidthStyle.fixedWidth=SPRITE_RECT_WIDTH + (SPRITE_MARGIN * 2);
			}
			return _verticalScrollViewFixedWidthStyle;
		}
		set {
			_verticalScrollViewFixedWidthStyle = value;
		}
	}	
}
