using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFTFlexibleBorderSpriteWindow : UFTSpriteWindowBase {
	private UFTAtlasEntryMetadata spriteMetadata;
	private UFTBorderWidth vertexBorderWidth;
	private UFTBorderWidth uvBorderWidth;
	private bool syncUVBorderWithVertexBorder;
	
	[MenuItem ("Window/UFT Flexible Border Sprite")]
    static void ShowWindow () {    		
		UFTFlexibleBorderSpriteWindow window = EditorWindow.GetWindow <UFTFlexibleBorderSpriteWindow>("UFT Flexible Border Sprite");
		window.initializeAtlasMetadataAndMaterial();			
    }
	
	public override void initializeAtlasMetadataAndMaterial(){
		base.initializeAtlasMetadataAndMaterial();	
	}
	
	void OnGUI(){		
		renderAtlasMetadata();	
		
		EditorGUILayout.BeginHorizontal();
		showMetadataGui(ref spriteMetadata,"Sprite");
		
		renderBorderWidth("uv border width",ref uvBorderWidth);
		EditorGUILayout.BeginVertical();
		renderBorderWidth("vertex border width",ref vertexBorderWidth);
		syncUVBorderWithVertexBorder = EditorGUILayout.Toggle("sync UV with Vertex values",syncUVBorderWithVertexBorder);
		if (syncUVBorderWithVertexBorder)
			vertexBorderWidth.getValuesFrom( uvBorderWidth);
		
		EditorGUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
	
		renderSizeMaterialGenerateButton();
	}

	public override void onGenerateButton(){	
	 	Mesh mesh=UFTFlexibleBorderMeshUtil.createBorderMesh(base.width,base.height,vertexBorderWidth,uvBorderWidth,spriteMetadata);
		GameObject go=new GameObject(base.buttonName);
		go.AddComponent<MeshFilter>().sharedMesh=mesh;
		AssetDatabase.CreateAsset(mesh, AssetDatabase.GenerateUniqueAssetPath("Assets/"+base.buttonName+".asset") );
        AssetDatabase.SaveAssets();	
		UFTFlexibleBorderSprite flexibleSprite= go.AddComponent<UFTFlexibleBorderSprite>();		
		flexibleSprite.uvBorderWidth     = uvBorderWidth      ;
		flexibleSprite.vertexBorderWidth = vertexBorderWidth  ;
		flexibleSprite.width             = base.width         ;
		flexibleSprite.height            = base.height        ;
		flexibleSprite.atlasMetadata     = base.atlasMetadata ;
		flexibleSprite.spriteMetadata    = spriteMetadata     ;
		MeshRenderer renderer= go.AddComponent<MeshRenderer>();
		renderer.material = base.buttonMaterial;
		
	}
	
	void renderBorderWidth(string name,ref UFTBorderWidth borderWidth){
		if (borderWidth==null)
			borderWidth = new UFTBorderWidth();
		int boxWidth=50;
		EditorGUILayout.BeginVertical();
			EditorGUILayout.LabelField(name+":");
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("",GUILayout.Width(boxWidth));
				borderWidth.top = EditorGUILayout.IntField((int)borderWidth.top,GUILayout.Width(boxWidth));
				EditorGUILayout.LabelField("",GUILayout.Width(boxWidth));
			EditorGUILayout.EndHorizontal();
		
			EditorGUILayout.BeginHorizontal();
				borderWidth.left = EditorGUILayout.IntField((int)borderWidth.left,GUILayout.Width(boxWidth));
				EditorGUILayout.LabelField("",GUILayout.Width(boxWidth));	
				borderWidth.right = EditorGUILayout.IntField((int)borderWidth.right,GUILayout.Width(boxWidth));
			EditorGUILayout.EndHorizontal();
				
			EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("",GUILayout.Width(boxWidth));
				borderWidth.down = EditorGUILayout.IntField((int)borderWidth.down,GUILayout.Width(boxWidth));
				EditorGUILayout.LabelField("",GUILayout.Width(boxWidth));			
			EditorGUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
	}
	
}
